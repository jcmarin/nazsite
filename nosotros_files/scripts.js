$(document).ready(function() {
	$("#slideshow").css("overflow", "hidden");

	$("ul#slides").cycle({
		fx: 'fade',
		pause: 1,
		prev: '#prev',
		next: '#next'
	});
	
	$("#slideshow").hover(function() {
		$("ul#nav").fadeIn();
	},
		function() {
		$("ul#nav").fadeOut();
	});

});
/*
     FILE ARCHIVED ON 15:19:29 Jan 09, 2019 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 00:56:13 Nov 01, 2019.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  exclusion.robots.policy: 0.136
  captures_list: 98.439
  load_resource: 1352.465
  RedisCDXSource: 0.659
  exclusion.robots: 0.147
  PetaboxLoader3.datanode: 908.644 (5)
  PetaboxLoader3.resolve: 504.691 (2)
  CDXLines.iter: 14.916 (3)
  LoadShardBlock: 79.804 (3)
  esindex: 0.011
*/